/*
 * lw_btree.h
 *
 *  Created on: 2013/09/06
 *      Author: root
 */

#ifndef LW_BTREE_H_
#define LW_BTREE_H_

#define BI_TREE_CHILD_NUM 2
#define BI_TREE_INDEX_MAX_LEN 3

typedef struct TreeNode_t
{
    void *pData;                  //������
    struct TreeNode_t *Child[BI_TREE_CHILD_NUM];  //����
}BT_NODE_ST;

typedef struct TreeIndex_t
{
    UINT32 auiIndex[BI_TREE_INDEX_MAX_LEN];
    UINT32 uiIndexLen;
}BT_INDEX_ST;

typedef int(*BTreeFuncCheck)(void *TreeData, void *InData);
typedef int(*BTreeFuncDeal)(void *TreeData, void *InData, void *OutData);
typedef int(*BTreeDelFunc)(IN void * pInData, BT_NODE_ST *pstNode);

VOID BT_GetBTreeInfo
(
    IN BT_NODE_ST *pstRoot,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeFuncDeal FuncDeal,
    OUT void *pOutData
);
VOID BT_DelBTreeByCheck
(
    IN BT_NODE_ST *pstRoot,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeDelFunc FuncDeal
);
BT_NODE_ST * BT_FindBTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex);

int BT_DelTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex, IN void * pInData, IN BTreeDelFunc FuncDel);
int BTreeDelFuncCommon(void *pInData, BT_NODE_ST *pstNode);
BT_NODE_ST * BT_AddBTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex);



#endif /* SYSLOG_H_ */
