
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <arpa/inet.h>
#include <lw_type.h>
#include <lw_drv_pub.h>
#include <lw_btree.h>
#include <lw_syslog.h>
#include <sys_memory.h>
#include "proto_mstp_def.h"
#include "ipc_protocol.h"


#define BT_INDEX_BIT(index, offset, bit) \
do{ \
    int _i = (offset)/32; \
    int _offset = (offset) % 32; \
    (bit) = (index[_i]>>_offset) & (0x1);\
}while(0)

static BT_NODE_ST * MallocBTree()
{
    BT_NODE_ST *pstDate = NULL;
    pstDate =(BT_NODE_ST *)MEM_MALLOC(sizeof(BT_NODE_ST));
    if(NULL != pstDate)
    {
        pstDate->pData=NULL;
        memset(pstDate->Child, 0, sizeof(pstDate->Child));
    }

    return pstDate;
}


/*==================================================================
* Function	: BT_AddBTree
* Description	:
* Input Para	:  BT_NODE_ST *pstRoot  ---- 根节点
                   BT_INDEX_ST *pstIndex ---- 索引
* Output Para	:
* Return Value:
==================================================================*/
BT_NODE_ST * BT_AddBTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex)
{
    UINT32 i = 0;
    UINT8 uiIndex = 0;
    BT_NODE_ST *pstNode = pstRoot;
    if((NULL == pstRoot)|| (NULL == pstIndex))
    {
        IC_SendErr(SYSTEM, "%s:%d Point is NULL", __FUNCTION__, __LINE__);
        return NULL;
    }

    if(pstIndex->uiIndexLen > (sizeof(pstIndex->auiIndex)*8))
    {
        IC_SendErr(SYSTEM, "%s:%d index is too long, len=%d", __FUNCTION__, __LINE__, pstIndex->uiIndexLen);
        return NULL;
    }

    for(i = 0; i < pstIndex->uiIndexLen; i++)
    {
        BT_INDEX_BIT(pstIndex->auiIndex, i, uiIndex);
        if(pstNode->Child[uiIndex] == NULL)
        {
            pstNode->Child[uiIndex] = MallocBTree();
            if(NULL == pstNode->Child[uiIndex])
            {
                IC_SendErr(SYSTEM, "%s:%d malloc error", __FUNCTION__, __LINE__);
                return NULL;
            }
        }
        pstNode = pstNode->Child[uiIndex];
    }

    return pstNode;
}
#if 0
#endif
int BTreeDelFuncCommon(void *pInData, BT_NODE_ST *pstNode)
{
    if((NULL != pstNode) && (NULL != pstNode->pData))
    {
        MEM_FREE(pstNode->pData);
        pstNode->pData = NULL;
    }
    return IPC_STATUS_OK;
}

static int DelTreeHandle(IN BT_NODE_ST *pstNode, IN BT_INDEX_ST *pstIndex, IN void * pInData, IN UINT32 uiOffset, IN BTreeDelFunc FuncDel)
{
    UINT8 uiIndex = 0;

    if(uiOffset >= pstIndex->uiIndexLen)
    {
        FuncDel(pInData, pstNode);
    }
    else
    {
        BT_INDEX_BIT(pstIndex->auiIndex, uiOffset, uiIndex);
        if(pstNode->Child[uiIndex] == NULL)
        {
            return DRV_OK;
        }
        uiOffset++;
        DelTreeHandle(pstNode->Child[uiIndex], pstIndex, pInData, uiOffset, FuncDel);

        /* 如果该节点下已没有内容，则删除该节点 */
        if((pstNode->Child[uiIndex]->pData == NULL) &&
           (pstNode->Child[uiIndex]->Child[0] == NULL) &&
           (pstNode->Child[uiIndex]->Child[1] == NULL))
        {
            MEM_FREE(pstNode->Child[uiIndex]);
            pstNode->Child[uiIndex] = NULL;
        }
    }

    return DRV_OK;
}


/*==================================================================
* Function	: BT_DelTree
* Description	: 按照索引删除节点
* Input Para	:BT_NODE_ST *pstRoot --- 根节点
                 BT_INDEX_ST *pstIndex --- 索引
                 void * pInData  --- deal函数使用，可以为NULL
                 BTreeDelFunc FuncDel  ----- 删除处理函数
* Output Para	:
* Return Value:
==================================================================*/
int BT_DelTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex, IN void * pInData, IN BTreeDelFunc FuncDel)
{
    UINT32 uiOffset = 0;

    if((NULL == pstRoot)|| (NULL == pstIndex))
    {
        IC_SendErr(SYSTEM, "%s:%d Point is NULL", __FUNCTION__, __LINE__);
        return DRV_ERR_PARA;
    }
    DelTreeHandle(pstRoot, pstIndex, pInData,uiOffset, FuncDel);
    return DRV_OK;
}
#if 0
#endif

/*==================================================================
* Function	: BT_FindBTree
* Description	:
* Input Para	:BT_NODE_ST *pstRoot --- 根节点
                 BT_INDEX_ST *pstIndex --- 索引
* Output Para	:
* Return Value:
==================================================================*/
BT_NODE_ST * BT_FindBTree(IN BT_NODE_ST *pstRoot, IN BT_INDEX_ST *pstIndex)
{
    UINT32 i = 0;
    UINT8 uiIndex = 0;
    BT_NODE_ST *pstNode = pstRoot;
    if((NULL == pstRoot)|| (NULL == pstIndex))
    {
        IC_SendErr(SYSTEM, "%s:%d Point is NULL", __FUNCTION__, __LINE__);
        return NULL;
    }

    if(pstIndex->uiIndexLen > (sizeof(pstIndex->auiIndex)*8))
    {
        IC_SendErr(SYSTEM, "%s:%d index is too long, len=%d", __FUNCTION__, __LINE__, pstIndex->uiIndexLen);
        return NULL;
    }

    for(i = 0; i < pstIndex->uiIndexLen; i++)
    {
        BT_INDEX_BIT(pstIndex->auiIndex, i, uiIndex);
        if(pstNode->Child[uiIndex] == NULL)
        {
            return NULL;
        }
        pstNode = pstNode->Child[uiIndex];
    }

    return pstNode;
}
#if 0
#endif
/*==================================================================
* Function	: GetBTreeInfo
* Description	:
* Input Para	:
* Output Para	:
* Return Value:  如果返回值为 FALSE 则结束递归
==================================================================*/
int GetBTreeInfo
(
    IN BT_NODE_ST *pstNode,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeFuncDeal FuncDeal,
    OUT void *pOutData
)
{
    int ret = 0;
    int i = 0;
    if(NULL != pstNode->pData)
    {
        if(TRUE == FuncCheck(pstNode->pData, pInData))
        {
            ret = FuncDeal(pstNode->pData, pInData, pOutData);
            if(ret == FALSE)
            {
                return FALSE;
            }
        }
    }

    for(i = 0; i< BI_TREE_CHILD_NUM; i++)
    {
        if(pstNode->Child[i] != NULL)
        {
            ret = GetBTreeInfo(pstNode->Child[i], pInData, FuncCheck, FuncDeal, pOutData);
            if(ret == FALSE)
            {
                return FALSE;
            }
        }
    }

    return TRUE;
}



/*==================================================================
* Function	: BT_GetBTreeInfo
* Description	: 根据条件查找二叉树
* Input Para	:BT_NODE_ST *pstRoot --- 根节点
                 void * pInData  --- deal函数使用，可以为NULL
                 BTreeFuncCheck FuncCheck ---- 判断条件是否成立
                 BTreeDelFunc FuncDel  ----- 处理函数，返回FALSE会跳出遍历查找
* Output Para	: void *pOutData  ---- out数据
* Return Value:
==================================================================*/
VOID BT_GetBTreeInfo
(
    IN BT_NODE_ST *pstRoot,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeFuncDeal FuncDeal,
    OUT void *pOutData
)
{
    BT_NODE_ST *pstNode = pstRoot;
    if(NULL == pstRoot)
    {
        IC_SendErr(SYSTEM, "%s:%d Point is NULL", __FUNCTION__, __LINE__);
        return;
    }

    GetBTreeInfo(pstNode, pInData, FuncCheck, FuncDeal, pOutData);

    return;
}
#if 0
#endif
/*==================================================================
* Function	: GetBTreeInfo
* Description	:
* Input Para	:
* Output Para	:
* Return Value:  如果返回值为 FALSE 则结束递归
==================================================================*/
int DelBTreeByCheck
(
    IN BT_NODE_ST *pstNode,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeDelFunc FuncDeal
)
{
    int ret = 0;
    int i = 0;
    if(NULL != pstNode->pData)
    {
        if(TRUE == FuncCheck(pstNode->pData, pInData))
        {
            ret = FuncDeal(pInData, pstNode);
            if(ret == FALSE)
            {
                return FALSE;
            }
        }
    }

    for(i = 0; i< BI_TREE_CHILD_NUM; i++)
    {
        if(pstNode->Child[i] != NULL)
        {
            ret = DelBTreeByCheck(pstNode->Child[i], pInData, FuncCheck, FuncDeal);
            /* 如果该节点下已没有内容，则删除该节点 */
            if((pstNode->Child[i]->pData == NULL) &&
               (pstNode->Child[i]->Child[0] == NULL) &&
               (pstNode->Child[i]->Child[1] == NULL))
            {
                MEM_FREE(pstNode->Child[i]);
                pstNode->Child[i] = NULL;
            }
            if(ret == FALSE)
            {
                return FALSE;
            }
        }
    }

    return TRUE;
}

/*==================================================================
* Function	: BT_DelBTreeByCheck
* Description	: 根据条件删除节点，会遍历所有节点
* Input Para	:BT_NODE_ST *pstRoot --- 根节点
                 void * pInData  --- deal函数使用，可以为NULL
                 BTreeFuncCheck FuncCheck ---- 判断条件是否成立
                 BTreeDelFunc FuncDel  ----- 处理函数，返回FALSE会跳出遍历查找
* Output Para	:
* Return Value:
==================================================================*/
VOID BT_DelBTreeByCheck
(
    IN BT_NODE_ST *pstRoot,
    IN void * pInData,
    IN BTreeFuncCheck FuncCheck,
    IN BTreeDelFunc FuncDeal
)
{
    BT_NODE_ST *pstNode = pstRoot;
    if(NULL == pstRoot)
    {
        IC_SendErr(SYSTEM, "%s:%d Point is NULL", __FUNCTION__, __LINE__);
        return;
    }

    DelBTreeByCheck(pstNode, pInData, FuncCheck, FuncDeal);

    return;
}

